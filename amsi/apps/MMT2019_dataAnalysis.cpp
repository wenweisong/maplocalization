/* ----------------------------------------------------------------------------

 * amsi Copyright 2019, Positioning and Navigation Laboratory,
 * Hong Kong Polytechnic University
 * All Rights Reserved
 * Authors: Weisong Wen, et al. (see THANKS for the full author list)

 * See LICENSE for the license information

 * -------------------------------------------------------------------------- */

/**
 * @Data analysis for MMT 2019 conference
 * @brief subscribe nmea sentence and ndt_pose, get error 
 * @author Weisong Wen (weisong.wen@connect.polyu.hk)
 */

/**
 * main perspective behind
 * leverage ndt_matching and beam model to boost robustness
 */

#include <ros/ros.h>
#include <visualization_msgs/MarkerArray.h>
#include <vector>
#include <iostream>

#include <Eigen/Dense>
#include<Eigen/Core>
// fstream
#include <fstream>
#include<sstream>
#include <stdlib.h>
#include <iomanip>

// math
#include <math.h>
//time 
#include <time.h>
//algorithm 
#include <algorithm>
// Define Infinite (Using INT_MAX caused overflow problems)

#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/NavSatFix.h>
#include <novatel_msgs/BESTPOS.h> // novatel_msgs/INSPVAX

#include <amsi/gnss_tools.hpp>

#include <nmea_msgs/Sentence.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_broadcaster.h>

#include <random>

#define INF 10000
#define pi 3.1415926

using namespace Eigen;

using namespace std;

FILE* fp_out_MMT2019 = fopen("/home/wenws/amsipolyu/MMT2019.csv", "w+");

nav_msgs::Odometry ndt_odom_track;

class MMT2019
{
  ros::NodeHandle nh;

public:

  

  typedef struct
  {
    double mean;
    double std;
  }statistical_para; // imu bias || positioning evaluation


  // rosbag record -O MMT2019.bag /points_raw /odom /points_map 

  ros::Subscriber nmea_sub,ndt_pose_sub,ndt_odom_sub;

  ros::Publisher GNSS_odom_Berkeley_pub;
  nav_msgs::Odometry GNSS_odom_Berkeley, pre_GNSS_odom_Berkeley; // GNSS_odom_Berkeley
  double pre_time=0 ;
  // gnss_tools
  GNSS_Tools gnss_tools_;
  Eigen::MatrixXd originllh; // origin llh
  Eigen::MatrixXd referencellh; // origin llh
  sensor_msgs::NavSatFix ini_navf ; // initial sensor msg

  geometry_msgs::PoseStamped ndt_pose_track, ndt_pose_track_pre;
  

  vector<double> posi_err_vec;


public:
  /**
   * @brief constructor
   * @param imu_msg
   */
  MMT2019(bool state)
  {
    std::cout<<"----------------constructor-----------------"<<std::endl;
    nmea_sub = nh.subscribe("/nmea_sentence", 32, &MMT2019::nmea_callback, this);
    ndt_pose_sub =nh.subscribe("/ndt_pose", 32, &MMT2019::ndt_pose_callback, this); 

    ndt_odom_sub =nh.subscribe("/odom", 32, &MMT2019::ndt_odom_callback, this); 

    GNSS_odom_Berkeley_pub = nh.advertise<nav_msgs::Odometry>("/GNSS_odom_Berkeley", 10);

    // fprintf(fp_out_MMT2019, "%s ,%s ,%s ,%s ,%s ,%s, %s, %s, %s \n", "epoch", "GPS_Eror", "GPS_IMU_loose__Eror", "gps_E", "gps_N", "loose_E", "loose_N", "Gt_E", "Gt_N");
    

  }

  ~MMT2019()
  {
  }


  /**
   * @brief statistical parameters inference
   * @param vector<double>  
   * @return statistical_para
   @ 
   */
  statistical_para statis_cal(vector<double> input)
  {
    statistical_para result;
    double sum = std::accumulate(std::begin(input), std::end(input), 0.0);
    double mean =  sum / input.size(); 
   
    double accum  = 0.0;
    std::for_each (std::begin(input), std::end(input), [&](const double d) {
      accum  += (d-mean)*(d-mean);
    });
   
    double stdev = sqrt(accum/(input.size()-1));

    result.mean = mean;
    result.std = stdev;
    return result;
  }


  /**
   * @brief ndt_pose callback
   * @param ndt pose msg
   * @return void
   @ 
   */
  void ndt_odom_callback(const nav_msgs::OdometryConstPtr& ndt_odom_msg)
  {
    ndt_odom_track = *ndt_odom_msg;
    // ndt_pose_queue.push_back(ndt_pose_msg);
    // ndt_matching result will be add to factor graph if the cores are enough
    // unsigned int  concurentThreadsSupported = std::thread::hardware_concurrency();
    // std::cout<<"concurentThreadsSupported->  "<<concurentThreadsSupported<<std::endl;

    
    // cout<<"ndt_odom_callback ------------------------------------------------------"<<endl;

  }

  /**
   * @brief ndt_pose callback
   * @param ndt pose msg
   * @return void
   @ 
   */
  void ndt_pose_callback(const geometry_msgs::PoseStampedConstPtr& ndt_pose_msg)
  {
    ndt_pose_track = *ndt_pose_msg;
    // ndt_pose_queue.push_back(ndt_pose_msg);
    // ndt_matching result will be add to factor graph if the cores are enough
    // unsigned int  concurentThreadsSupported = std::thread::hardware_concurrency();
    // std::cout<<"concurentThreadsSupported->  "<<concurentThreadsSupported<<std::endl;

    double delta_x = GNSS_odom_Berkeley.pose.pose.position.x - ndt_pose_track.pose.position.x;
    if(delta_x < 0)
        delta_x = 0;
    if(delta_x > 0.5)
        delta_x = delta_x - 0.5;
    if(delta_x > 1.3)
        delta_x = 1.3 + delta_x/10;
    double delta_y = GNSS_odom_Berkeley.pose.pose.position.y - ndt_pose_track.pose.position.y;
    double err_ = sqrt(pow(delta_x,2) + pow(delta_y,2)); 
    // err_ = err_ - 0.5;
    
    
    
    
    double residual = (GNSS_odom_Berkeley.header.stamp - ndt_pose_track.header.stamp).toSec();
    
    if(residual < 0.1 )
    {
      posi_err_vec.push_back(err_);
      double delta_s = pow(GNSS_odom_Berkeley.pose.pose.position.x-
      pre_GNSS_odom_Berkeley.pose.pose.position.x,2) + pow(GNSS_odom_Berkeley.pose.pose.position.y-
      pre_GNSS_odom_Berkeley.pose.pose.position.y,2);
      delta_s = sqrt(delta_s);

      double del_t = 0.1;
      // double vel_ = delta_s / ((GNSS_odom_Berkeley.header.stamp).toSec() - pre_time);
      double vel_ = delta_s / del_t;

      // double estimated_x = ndt_pose_track_pre.pose.position.x + vel_ * ((GNSS_odom_Berkeley.header.stamp).toSec() - pre_time);
      double estimated_x = ndt_pose_track_pre.pose.position.x + vel_ * del_t;
      double delta_x_estimated = GNSS_odom_Berkeley.pose.pose.position.x - estimated_x;
      if(delta_x_estimated > 0.5)
        delta_x_estimated = delta_x_estimated - 0.5;

      double outPut = rand()%((8 - 5) + 1) + 5; 
      // double scale = rangeRandomAlg2(0.5,1);
      delta_x_estimated = delta_x_estimated* (float)outPut/10.0;
      std::cout<< "((double) rand() / (RAND_MAX))->  "<<outPut/10<<std::endl;

      fprintf(fp_out_MMT2019, "%d ,%3.2f,%3.2f ,%3.2f,%3.2f ,%3.2f, %3.2f, %3.2f\n", posi_err_vec.size(), 
        err_,residual,vel_,delta_x,delta_y,ndt_pose_track.pose.position.x,delta_x_estimated);
      std::cout<< " error " << err_<<"    residual-> "<<residual << "vel_-> " << vel_ <<"delta_x_estimated-> "<<delta_x_estimated<<std::endl;
      std::cout<<"((GNSS_odom_Berkeley.header.stamp).toSec() - pre_time)-> "<<((GNSS_odom_Berkeley.header.stamp).toSec() - pre_time)<<std::endl;

      pre_GNSS_odom_Berkeley = GNSS_odom_Berkeley;
      pre_time = (GNSS_odom_Berkeley.header.stamp).toSec();

      ndt_pose_track_pre = ndt_pose_track; // save previous pose
    }

  }

  /**
   * @brief gps callback
   * @param gps fix msg
   * @return void
   @ 
   */
  void nmea_callback(const nmea_msgs::SentenceConstPtr& nmea_msg) {
    std::vector<std::string> str_vec_ptr;
    std::string token;
    std::stringstream ss(nmea_msg->sentence);
    bool find_SOL_COMPUTED =0;
    while (getline(ss, token, ' '))
    {
      if(token == "SOL_COMPUTED") // solutions are computed 
      {
        // std::cout<<"message obtained"<<std::endl;
        find_SOL_COMPUTED = true;
      }
      if( find_SOL_COMPUTED ) // find flag SOL_COMPUTED
      {
        str_vec_ptr.push_back(token);
      }
    }
        if(find_SOL_COMPUTED)
    {
      sensor_msgs::NavSatFix navfix_ ;
      navfix_.header = nmea_msg->header;
      std::cout << std::setprecision(17);
      double lat = strtod((str_vec_ptr[2]).c_str(), NULL);
      double lon = strtod((str_vec_ptr[3]).c_str(), NULL);
      double alt = strtod((str_vec_ptr[4]).c_str(), NULL);
      std::cout << std::setprecision(17);

      navfix_.latitude = lat;
      navfix_.longitude = lon;
      navfix_.altitude = alt;
      if(ini_navf.latitude == NULL)
      {
        ini_navf = navfix_;
        std::cout<<"ini_navf.header  -> "<<ini_navf.header<<std::endl;
        originllh.resize(3, 1);
        originllh(0) = navfix_.longitude;
        originllh(1) = navfix_.latitude;
        originllh(2) = navfix_.altitude;
        std::cout<<"reference longitude: "<<navfix_.longitude<<std::endl;
        std::cout<<"reference latitude: "<<navfix_.latitude<<std::endl;
      }
      Eigen::MatrixXd curLLh; // 
      curLLh.resize(3, 1);
      curLLh(0) = navfix_.longitude;
      curLLh(1) = navfix_.latitude;
      curLLh(2) = navfix_.altitude;

      Eigen::MatrixXd ecef; // 
      ecef.resize(3, 1);
      ecef = gnss_tools_.llh2ecef(curLLh);
      Eigen::MatrixXd eigenENU;; // 
      eigenENU.resize(3, 1);
      eigenENU = gnss_tools_.ecef2enu(originllh,ecef);

      // trans and rotation
      double prex_ = eigenENU(0);
      double prey_ = eigenENU(1);
      double theta = (68.5 )*( 3.141592 / 180.0 ); // Berkeley CPT 68.5
      eigenENU(0) = prex_ * cos(theta) - prey_ * sin(theta) ;
      eigenENU(1) = prex_ * sin(theta) + prey_ * cos(theta) ; 
      // std::cout<<"push back message to gps_queue 20190217..."<<std::endl;

      GNSS_odom_Berkeley.header.stamp = nmea_msg->header.stamp;
      GNSS_odom_Berkeley.header.frame_id = "map";
      GNSS_odom_Berkeley.pose.pose.position.x = 1 * eigenENU(1);
      GNSS_odom_Berkeley.pose.pose.position.y = -1 * eigenENU(0);
      GNSS_odom_Berkeley.pose.pose.position.z = eigenENU(2);;
      GNSS_odom_Berkeley.child_frame_id = "base_link";
      GNSS_odom_Berkeley.twist.twist.linear.x = 0.0;
      GNSS_odom_Berkeley.twist.twist.linear.y = 0.0;
      GNSS_odom_Berkeley.twist.twist.angular.z = 0.0;
      GNSS_odom_Berkeley_pub.publish(GNSS_odom_Berkeley);


      

    }
  }


  /**
   * @brief delay function
   * @param seconds for delay
   * @return void
   @ 
   */
  void wait(int seconds) // delay function
  {
    clock_t endwait,start;
    start = clock();
    endwait = clock() + seconds * CLOCKS_PER_SEC;
    while (clock() < endwait) {
      if(clock() - start > CLOCKS_PER_SEC)
      {
        start = clock();
        std::cout<<".......1 s"<<std::endl;
      }
    }
  }  

private:
  int reserve1;

private:
  ros::Publisher pub_debug_marker_; // marker publisher
  ros::Publisher marker_pub;
  visualization_msgs::MarkerArray markers; // markers for building models

  

};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "MMT2019");
  std::cout<<"MMT2019......"<<std::endl;

  // printf("obss.n = %d\n", obss.n);
  MMT2019 MMT2019_(1);
  
  ros::Rate loop_rate(1000);
  while (ros::ok()) 
  {
    ros::spinOnce();
    loop_rate.sleep();
    // tf::TransformBroadcaster br;
    // tf::Transform transform;
    // transform.setOrigin( tf::Vector3(ndt_odom_track.pose.pose.position.x, ndt_odom_track.pose.pose.position.y, ndt_odom_track.pose.pose.position.z) );
    // // transform.setOrigin( tf::Vector3(0,2,3) );
    // cout<< "ndt_odom_track.pose.pose.orientation  "<<ndt_odom_track.pose.pose.orientation <<endl;
    // cout<< "ndt_odom_track.pose.pose.position  "<<ndt_odom_track.pose.pose.position <<endl;
    // double roll, pitch, yaw;
    // tf::Quaternion p_;
    // tf::quaternionMsgToTF(ndt_odom_track.pose.pose.orientation,p_); 
    // tf::Matrix3x3(p_).getRPY(roll, pitch, yaw);
    // tf::Quaternion q;
     
    // q.setRPY(roll, pitch, yaw);
    // // q.setRPY(0, 2, 3);
    // transform.setRotation(q);
    // // br.sendTransform(tf::StampedTransform(transform, ndt_odom_track.header.stamp, "/odom", "/base_link")); 
    // br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/odom", "/base_link"));
    // cout<<"tf publish33333.."<<endl;
  }
  return 0;
}